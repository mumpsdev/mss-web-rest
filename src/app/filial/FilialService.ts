import { tMsg } from './../../util/Values';
import { msgFilial } from './../../util/MessageTranslate';
import { Data } from './../../util/Data';
import * as express from "express";
import FilialModel  from "./FilialModel";

class FilialService{

    public async getAll(req, res){
        let data = new Data();
 
        try {
            let filter = data.createFilter(req);
            let query = FilialModel.find(filter);
            query.populate("organizacao", "fantasia");
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoListarFiliais);
            console.log("Erro: " + error);
            res.status(500).json(data);

        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            data.obj = await FilialModel.create(req.body)
            data.addMsg(tMsg.DANGER, msgFilial.filialInseridaComSucesso);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoInserirFilial);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await FilialModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgFilial.filialAlteradaComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgFilial.erroAoLocalizarFilial);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoAlterarFilial);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await FilialModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgFilial.filialRemovidaComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgFilial.erroAoLocalizarFilial);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoRemoverFilial);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await FilialModel.findById(id);
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgFilial.erroAoLocalizarFilial);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoLocalizarFilial);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new FilialService();