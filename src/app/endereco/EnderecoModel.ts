import * as mongoose from "mongoose";

export let EnderecoSchema = new mongoose.Schema({
    cep: {
        type: String,
    },
    logradouro:{
        type: String,
        required: true
    },
    numero:{
        type: String,
        required: true
    },
    bairro:{
        type: String
    },
    cidade:{
        type: String
    },
    estado:{
        type: String
    },
    pais:{
        type: String
    }
});

export default mongoose.model("Endereco", EnderecoSchema);