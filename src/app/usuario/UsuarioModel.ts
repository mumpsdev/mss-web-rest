import * as mongoose from "mongoose";

export let UsuarioSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    senha: {
        type: String,
        unique: true
    },
    nome: {
        type: String,
        required: true,
    },
    token:{
        type: String
    },
    isProprietario: {
        type: Boolean,
        default: false
    },
    organizacoes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Organizacao"
    }],
    perfis:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Perfil"
    }],
    dispositivos:[],
    dados_pessoais:[],
    contatos:[]
});

export default mongoose.model("Usuario", UsuarioSchema);