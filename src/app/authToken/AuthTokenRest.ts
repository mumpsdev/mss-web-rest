import { URLs } from '../../util/Values';
import AuthTokenService from "./AuthTokenService";
import * as express from "express";

class AuthTokenRest{
    constructor(){} 

    public setRoutes(exp: express.Application){
        exp.get(URLs.AUTH_LOGIN, AuthTokenService.getLoginAuth);
        exp.post(URLs.AUTH_AUTHENTICATE, AuthTokenService.authenticate);
        
        // exp.use(URLs.AUTH_ALL, AuthTokenService.validateToken);
    }
}

export default new AuthTokenRest();