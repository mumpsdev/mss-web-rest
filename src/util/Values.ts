export const URLs = {
    //Urls api.
    //Auth
    "AUTH_LOGIN": "/api/v1/loginAuth/:login",
    "AUTH_AUTHENTICATE": "/api/v1/authenticate",
    "AUTH_LOGGED": "/api/v1/loggedAuth",
    "AUTH_ALL": "/*",
    //Usuário
    "USUARIO": "/api/v1/usuario",
    "USUARIO_ID": "/api/v1/usuario/:id",
    //Organizacao
    "ORGANIZACAO": "/api/v1/organizacao",
    "ORGANIZACAO_ID": "/api/v1/organizacao/:id",    
    //Filial
    "FILIAL": "/api/v1/filial",
    "FILIAL_ID": "/api/v1/filial/:id",     
    //Perfil
    "PERFIL": "/api/v1/perfil",
    "PERFIL_ID": "/api/v1/perfil/:id",    
    //Acao
    "ACAO": "/api/v1/acao",
    "ACAO_ID": "/api/v1/acao/:id",
    //Endereco
    "ENDERECO": "/api/v1/endereco",
    "ENDERECO_ID": "/api/v1/endereco/:id",
    
}

export const tMsg = {//Tipos de mensagens
    "DANGER": "msgErro",
    "SUCCESS": "msgSuccesso",
    "INFO": "msgInfo",
    "ALERT": "msgAlert"
}

export const oMsg = {//Objetos de retorno
    "OBJ": "obj",
    "LIST": "list",
    "LIST_MSG": "listMsg",
    "PAGE": "page",
    "TOTAL_PAGES": "totalPages",
    "LIMIT": "limit",
    "RANGE_INIT": "rangeInit",
    "RANGE_END": "rangeEnd",
    "ASC": "asc",
    "DESC": "desc",
    "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
    "TOKEN": "x-access-token",
    "TIME": 84600,
    "SECRET": "mumpssolution"
}
