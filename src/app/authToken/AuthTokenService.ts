import { msgUsuario } from './../../util/MessageTranslate';
import { msgAuth } from '../../util/MessageTranslate';
import { tMsg, oMsg, secretToken } from '../../util/Values';
import UtilService from "../../util/UtilService"
import * as jwt from "jsonwebtoken";
import { Data } from '../../util/Data';
import { json } from 'body-parser';
import UsuarioModel from '../usuario/UsuarioModel';
import { url } from 'inspector';

class AuthTokenService{
    constructor(){
    }

    public async getLoginAuth(req, res){
        let data = new Data();
        let login = req.params.login;
        try {
            data.obj = await UsuarioModel.findOne({"login": login} , "login nome");
            if(data.obj){
				res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async authenticate(req, res){
        let data = new Data()
     
        try {
            let login = req.body.login
            let senha = req.body.senha
            senha = UtilService.encrypt(senha);
            let result: {[k: string]: any} = {}
            result.usuario = await UsuarioModel.findOne({"login": login, "senha": senha}, "login nome isProprietario")
                .populate({
                    path: "organizacoes", 
                    populate: {
                        path: "filiais"
                    }
                }).populate({
                    path: "perfis",
                    populate:{
                        path: "acoes"
                    }
                });
            
            if(!result.usuario){
                data.addMsg(tMsg.DANGER, msgAuth.loginhaOuSenhaInvalidos);
                res.sendStatus(401);
            }
            result.usuario['password'] = '';
            let acoes = data.getActionsUser(result.usuario);
            let dataBase = Date.now();

            let token = jwt.sign(
                {   iss : 'mss', // (Issuer) Origem do token
                    iat: Math.floor(dataBase), // (issueAt) Timestamp de quando o token foi gerado
                    exp : Math.floor(dataBase + ( 1 * 60 * 60 * 1000 ) ), //(Expiration) Timestamp de quando o token expira
                    sub : result.usuario._id, //(Subject) Entidade a quem o token pertence
                    login: result.usuario["login"],
                    acoes: acoes
                }, secretToken.SECRET);
            result[secretToken.TOKEN] = token;
            data.obj = result;
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.erroAoConsultarLoginDeAcesso)
            res.sendStatus(401).json(data)
        }
    }

    public async validateToken(req, res, next){
        let data = new Data()
        try {
            let token = req.body[secretToken.TOKEN] || req.query[secretToken.TOKEN] || req.headers[secretToken.TOKEN];
            if(token){
                let userLogged = jwt.verify(token, secretToken.SECRET)
                let acoes: Array<string> = userLogged["acoes"];
                let urlOriginal = data.getUrlPattern("/api/v1", req.originalUrl, true);
                let nomeUrlTipo = urlOriginal + "-" + req.method;
                if(!acoes || acoes.indexOf(nomeUrlTipo) < 0){
                    data.addMsg(tMsg.DANGER, msgAuth.acessoNegado);
                    res.status(401).json(data);
                }
                next();
            }else{
                data.addMsg(tMsg.DANGER, msgAuth.tokenNaoEnviado);
                res.status(401).json(data);
                return
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.tokenInvalido);
            // data.addMsg(tMsg.STRACE, error)
            res.status(500).json(data);
            return
        }    
        
    }

}

export default new AuthTokenService();