import { URLs } from './../../util/Values';
import EnderecoService  from './EnderecoService';
import * as express from "express";

class EnderecoRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.ENDERECO)
        .get(EnderecoService.getAll)
        .post(EnderecoService.create); 
        
        express.route(URLs.ENDERECO_ID)
        .get(EnderecoService.getById)
        .delete(EnderecoService.remove)
        .put(EnderecoService.update);
    }
}
export default new EnderecoRest();