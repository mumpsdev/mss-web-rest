import * as mongoose from "mongoose";

export let FilialSchema = new mongoose.Schema({
    fantasia:{
        type: String,
        required: true,
    },
    razaoSocial:{
        type: String,
        unique: true,
        required: true,
    }
});

export default mongoose.model("Filial", FilialSchema);