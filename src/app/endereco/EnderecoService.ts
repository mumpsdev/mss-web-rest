import { tMsg } from './../../util/Values';
import { msgEndereco } from './../../util/MessageTranslate';
import { Data } from './../../util/Data';
import * as express from "express";
import EnderecoModel  from "./EnderecoModel";

class EnderecoService{
    public async getAll(req, res){
        let data = new Data();
        try {
            let filter = data.createFilter(req);
            let query = EnderecoModel.find(filter);
            query.populate("acoes", "nome url");
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgEndereco.erroAoListarEnderecos);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            data.obj = await EnderecoModel.create(req.body);
            data.addMsg(tMsg.SUCCESS, msgEndereco.enderecoInseridoComSucesso);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgEndereco.erroAoInserirEndereco);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await EnderecoModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgEndereco.enderecoAlteradoComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgEndereco.erroAoLocalizarEndereco);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgEndereco.erroAoAlterarEndereco);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await EnderecoModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgEndereco.enderecoRemovidoComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgEndereco.erroAoLocalizarEndereco);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgEndereco.erroAoRemoverEndereco);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await EnderecoModel.findById(id).populate("acoes", "nome url");
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgEndereco.erroAoLocalizarEndereco);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgEndereco.erroAoLocalizarEndereco);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new EnderecoService();