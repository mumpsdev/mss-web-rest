import { tMsg } from './../../util/Values';
import { msgPerfil } from './../../util/MessageTranslate';
import { Data } from './../../util/Data';
import * as express from "express";
import PerfilModel  from "./PerfilModel";

class PerfilService{
    public async getAll(req, res){
        let data = new Data();
        try {
            let filter = data.createFilter(req);
            let query = PerfilModel.find(filter);
            query.populate("acoes", "nome url");
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoListarPerfis);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            data.obj = await PerfilModel.create(req.body);
            data.addMsg(tMsg.SUCCESS, msgPerfil.perfilInseridoComSucesso);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoInserirPerfil);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await PerfilModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgPerfil.perfilAlteradoComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgPerfil.erroAoLocalizarPerfil);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoAlterarPerfil);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await PerfilModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgPerfil.perfilRemovidoComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgPerfil.erroAoLocalizarPerfil);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoRemoverPerfil);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await PerfilModel.findById(id).populate("acoes", "nome url");
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgPerfil.erroAoLocalizarPerfil);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoLocalizarPerfil);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new PerfilService();