import { URLs } from './../../util/Values';
import PerfilService  from './PerfilService';
import * as express from "express";

class PerfilRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.PERFIL)
        .get(PerfilService.getAll)
        .post(PerfilService.create); 
        
        express.route(URLs.PERFIL_ID)
        .get(PerfilService.getById)
        .delete(PerfilService.remove)
        .put(PerfilService.update);
    }
}
export default new PerfilRest();