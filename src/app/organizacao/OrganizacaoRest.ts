import { URLs } from './../../util/Values';
import OrganizacaoService  from './OrganizacaoService';
import * as express from "express";

class OrganizacaoRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.ORGANIZACAO)
        .get(OrganizacaoService.getAll)
        .post(OrganizacaoService.create); 
        
        express.route(URLs.ORGANIZACAO_ID)
        .get(OrganizacaoService.getById)
        .delete(OrganizacaoService.remove)
        .put(OrganizacaoService.update);
    }
}
export default new OrganizacaoRest();