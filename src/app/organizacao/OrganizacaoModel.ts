import * as mongoose from "mongoose";

export let OrganizacaoSchema = new mongoose.Schema({
    fantasia:{
        type: String,
        required: true,
    },
    razaoSocial:{
        type: String,
        unique: true,
        required: true,
    }
    , filiais: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Filial' }]

});

export default mongoose.model("Organizacao", OrganizacaoSchema);