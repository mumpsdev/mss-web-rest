import { URLs } from './../../util/Values';
import FilialService  from './FilialService';
import * as express from "express";

class FilialRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.FILIAL)
        .get(FilialService.getAll)
        .post(FilialService.create); 
        
        express.route(URLs.FILIAL_ID)
        .get(FilialService.getById)
        .delete(FilialService.remove)
        .put(FilialService.update);
    }
}
export default new FilialRest();