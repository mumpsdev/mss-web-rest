export const msgGeneric = {
    "nenhumRegistroEncontrado": "NENHUM_REGISTRO_ENCONTRADO",
    "erroAoLocalizarRegistro"     : "ERRO_AO_LOCALIZAR_REGISTRO",
    "erroAoRemoverRegistro"     : "ERRO_AO_REMOVER_REGISTRO",
    "erroAoListarRegistros"     : "ERRO_AO_LISTAR_REGISTRO",
    "erroAoInserirRegistro"     : "ERRO_AO_INSERIR_REGISTRO",
    "erroAoAlterarRegistro"     : "ERRO_AO_ALTERAR_REGISTRO",
    "registroInseridoComSucesso": "REGISTRO_INSERIDO_COM_SUCESSO",
    "registroAlteradoComSucesso": "REGISTRO_ALTERADO_COM_SUCESSO",
    "registroRemovidoComSucesso": "REGISTRO_REMOVIDO_COM_SUCESSO",
}

export const msgAuth = {//Mensagens de retornos
    "erroAoConsultarLoginDeAcesso" : "ERRO_AO_CONSULTAR_LOGIN_DE_ACESSO",
    "tokenNaoEnviado": "TOKEN_NAO_ENVIADO",
    "loginInvalido" : "LOGIN_INVALIDO",
    "tokenInvalido" : "TOKEN_INVALIDO",
    "loginhaOuSenhaInvalidos": "LOGIN_OU_SENHA_INVALIDOS",
    "erroAoValidarToken": "ERRO_AO_VALIDAR_TOKEN",
    "nenhumLoginEncontrado": "NENHUM_LOGIN_USUARIO",
    "acessoNegado" : "ACESSO_NEGADO"
};

export const msgUsuario = {
    "erroAoListarUsuarios": "ERRO_AO_LISTAR_USUARIOS",
    "nenhumUsuarioEncontrado": "NENHUM_USUARIO_ENCONTRADO",
    "erroAoLocalizarUsuario": "ERRO_AO_LOCALIZAR_USUARIO",
    "erroAoInserirUsuario": "ERRO_AO_INSERIR_USUARIO",
    "erroAoAlterarUsuario": "ERRO_AO_ALTERAR_USUARIO",
    "erroAoRemoverUsuario": "ERRO_AO_REMOVER_USUARIO",
    "usuarioInseridoComSucesso": "USUARIO_INSERIDO_COM_SUCESSO",
    "usuarioAlteradoComSucesso": "USUARIO_ALTERADO_COM_SUCESSO",
    "usuarioRemovidoComSucesso": "USUARIO_REMOVIDO_COM_SUCESSO",
}

export const msgOrganizacao = {
    "erroAoListarOrganizacoes": "ERRO_AO_LISTAR_ORGANIZACOES",
    "nenhumaOrganizacaoEncontrada": "NENHUMA_ORGANIZACAO_ENCONTRADA",
    "erroAoLocalizarOrganizacao": "ERRO_AO_LOCALIZAR_ORGANIZACAO",
    "erroAoInserirOrganizacao": "ERRO_AO_INSERIR_ORGANIZACAO",
    "erroAoAlterarOrganizacao": "ERRO_AO_ALTERAR_ORGANIZACAO",
    "erroAoRemoverOrganizacao": "ERRO_AO_REMOVER_ORGANIZACAO",    
    "organizacaoCampoFantasiaObrigatorio": "ORGANIZACAO_CAMPO_FANTASIA_OBIRGATORIO",
    "organizacaoCampoRazaoSocialObrigatorio": "ORGANIZACAO_CAMPO_RAZAO_SOCIAL_OBRIGATORIO",
    "organizacaoCampoChaveGrupoContatoObrigatorio": "ORGANIZACAO_CAMPO_CHAVE_GRUPO_CONTATO_OBRIGATORIO",
    "organizacaoCampoChaveGrupoDocumentoObrigatorio": "ORGANIZACAO_CAMPO_CHAVE_GRUPO_DOCUMENTO_OBRIGATORIO",
    "organizacaoCampoStatusInvalido": "ORGANIZACAO_CAMPO_STATUS_INVALIDO",
    "organizacaoInseridaComSucesso": "ORGANIZACAO_INSERIDA_COM_SUCESSO",
    "organizacaoAlteradaComSucesso": "ORGANIZACAO_ALTERADA_COM_SUCESSO",
    "organizacaoRemovidaComSucesso": "ORGANIZACAO_REMOVIDA_COM_SUCESSO", 
    "organizacaoDeveSerUnica": "ORGANIZACAO_DEVE_SER_UNICA"   
}

export const msgFilial = {
    "erroAoListarFiliais": "ERRO_AO_LISTAR_FILIAIS",
    "nenhumaFilialEncontrada": "NENHUMA_FILIAL_ENCONTRADA",
    "erroAoLocalizarFilial": "ERRO_AO_LOCALIZAR_FILIAL",
    "erroAoInserirFilial": "ERRO_AO_INSERIR_FILIAL",
    "erroAoAlterarFilial": "ERRO_AO_ALTERAR_FILIAL",
    "erroAoRemoverFilial": "ERRO_AO_REMOVER_FILIAL",
    "erroAoListarFilial": "ERRO_AO_LISTAR_FILIAL",
    "filialCampoFantasiaObrigatorio": "FILIAL_CAMPO_FANTASIA_OBIRGATORIO",
    "filialCampoRazaoSocialObrigatorio": "FILIAL_CAMPO_RAZAO_SOCIAL_OBRIGATORIO",
    "filialCampoChaveGrupoContatoObrigatorio": "FILIAL_CAMPO_CHAVE_GRUPO_CONTATO_OBRIGATORIO",
    "filialCampoChaveGrupoDocumentoObrigatorio": "FILIAL_CAMPO_CHAVE_GRUPO_DOCUMENTO_OBRIGATORIO",
    "filialCampoOrganizacaoObrigatorio": "FILIAL_CAMPO_ORGANIZACAO_OBRIGATORIO",
    "filialCampoStatusInvalido": "FILIAL_CAMPO_STATUS_INVALIDO",
    "filialInseridaComSucesso": "FILIAL_INSERIDA_COM_SUCESSO",
    "filialAlteradaComSucesso": "FILIAL_ALTERADA_COM_SUCESSO",
    "filialRemovidaComSucesso": "FILIAL_REMOVIDA_COM_SUCESSO", 
    "filialDeveSerUnica": "FILIAL_DEVE_SER_UNICA",
    "filialAssociacaoOrganizacaoNaoEncontrada": "FILIAL_ASSOCIADO_ORGANIZACAO_NAO_ENCONTRADA"       
}

export const msgPerfil = {
    "erroAoListarPerfis": "ERRO_AO_LISTAR_PERFIS",
    "nenhumPerfilEncontrado": "NENHUM_PERFIL_ENCONTRADO",
    "erroAoLocalizarPerfil": "ERRO_AO_LOCALIZAR_PERFIL",
    "erroAoInserirPerfil": "ERRO_AO_INSERIR_PERFIL",
    "erroAoAlterarPerfil": "ERRO_AO_ALTERAR_PERFIL",
    "erroAoRemoverPerfil": "ERRO_AO_REMOVER_PERFIL",
    "perfilInseridoComSucesso": "PERFIL_INSERIDO_COM_SUCESSO",
    "perfilAlteradoComSucesso": "PERFIL_ALTERADO_COM_SUCESSO",
    "perfilRemovidoComSucesso": "PERFIL_REMOVIDO_COM_SUCESSO",
    "perfilDeveSerUnico": "PERFIL_DEVE_SER_UNICO",
    "perfilCampoNomeRangeObrigatorio": "PERFIL_CAMPO_NOME_RANGE_OBRIGATORIO",
    "perfilListaAcoesObrigatorio": "PERFIL_LISTA_ACOES_OBRIGATORIO"        
}

export const msgAcao = {
    "erroAoListarAcoes": "ERRO_AO_LISTAR_ACOES",
    "nenhumAAcaoEncontrada": "NENHUMA_ACAO_ENCONTRADA",
    "erroAoLocalizarAcao": "ERRO_AO_LOCALIZAR_ACAO",
    "erroAoInserirAcao": "ERRO_AO_INSERIR_ACAO",
    "erroAoAlterarAcao": "ERRO_AO_ALTERAR_ACAO",
    "erroAoRemoverAcao": "ERRO_AO_REMOVER_ACAO",
    "acaoInseridaComSucesso": "ACAO_INSERIDO_COM_SUCESSO",
    "acaoAlteradaComSucesso": "ACAO_ALTERADO_COM_SUCESSO",
    "acaoRemovidaComSucesso": "ACAO_REMOVIDO_COM_SUCESSO"
}

export const msgEndereco = {
    "erroAoListarEnderecos": "ERRO_AO_LISTAR_ENDERECOS",
    "nenhumAEnderecoEncontrada": "NENHUMA_ENDERECO_ENCONTRADA",
    "erroAoLocalizarEndereco": "ERRO_AO_LOCALIZAR_ENDERECO",
    "erroAoInserirEndereco": "ERRO_AO_INSERIR_ENDERECO",
    "erroAoAlterarEndereco": "ERRO_AO_ALTERAR_ENDERECO",
    "erroAoRemoverEndereco": "ERRO_AO_REMOVER_ENDERECO",
    "enderecoInseridoComSucesso": "ENDERECO_INSERIDO_COM_SUCESSO",
    "enderecoAlteradoComSucesso": "ENDERECO_ALTERADO_COM_SUCESSO",
    "enderecoRemovidoComSucesso": "ENDERECO_REMOVIDO_COM_SUCESSO"
}