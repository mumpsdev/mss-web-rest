import { tMsg } from './../../util/Values';
import { msgAcao } from './../../util/MessageTranslate';
import { Data } from '../../util/Data';
import * as express from "express";
import AcaoModel  from "./AcaoModel";

class AcaoService{

    public async getAll(req, res){
        let data = new Data();
 
        try {
            let filter = data.createFilter(req);
            let query = AcaoModel.find(filter);
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoListarAcoes);
            console.log("Erro: " + error);
            res.status(500).json(data);

        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            data.obj = await AcaoModel.create(req.body)
            data.addMsg(tMsg.DANGER, msgAcao.acaoInseridaComSucesso);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoInserirAcao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await AcaoModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgAcao.acaoAlteradaComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgAcao.erroAoLocalizarAcao);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoAlterarAcao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await AcaoModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgAcao.acaoRemovidaComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgAcao.erroAoLocalizarAcao);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoRemoverAcao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await AcaoModel.findById(id);
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgAcao.erroAoLocalizarAcao);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoLocalizarAcao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new AcaoService();