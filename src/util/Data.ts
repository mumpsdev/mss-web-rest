import { oMsg } from './Values';
import * as mongoose from "mongoose";
import * as express from "express";
import Validation from './Validation';
export class Data{
    public obj: Object;
    public list: Array<any>;
    public listMsg: Array<any>;   
    public populates: Array<any>;
    
    public page: number;  
    public totalPages: number;  
    public limit: number;  
    public rangeInit: number;  
    public rangeEnd: number;  
    public asc: Array<any>;  
    public desc: Array<any>;  
    public totalRows: number;  


    public addMsg(typeMsg: string, textMsg: string){
        if(!this.listMsg){
            this.listMsg = new Array();
        }
        this.listMsg.push({type: typeMsg, msg: textMsg});
    }

    public createFilter(request:express.Request): any{
        let filter = {};
        try {
            let obj = request.query;
            //Palavras que nâo entrarão no where.
            let notWhere: Array<string> = ["page", "limit", "asc", "desc", "fields", ];
            let LIMIT_PAGE = 10;
            // const SERVICO = Perfil.name.toLowerCase();   
            this.asc = obj.asc && obj.asc.indexOf(",") > -1 ? obj.asc.split(',') : obj.asc;
            this.desc = obj.desc && obj.desc.indexOf(",") > -1 ? obj.desc.split(',') : obj.desc;
            this.page = obj.page ? obj.page : undefined;
            this.limit = (obj.limit || this.page) ? parseInt(obj.limit) || LIMIT_PAGE : undefined;
            let filter = this.createFilters(obj, notWhere);
            return filter;
        } catch (error) {
            console.log(error);
        }
        return filter;
    }

    public async executeQuery(query: mongoose.Query<any>){
        try {
            let sorting = this.getCriteriaOrders(this.asc, this.desc, "id");
            if(this.page && this.limit){
                this.totalRows = await query.count();
                let skipe = (this.page-1) * this.limit;
                this.list = await query.skip(skipe).limit(this.limit).sort(sorting).exec("find");
                this.calcRanges();
            }else{
                this.list = await query.sort(sorting).exec("find");
            }
        } catch (error) {
            console.log(error);
        }
        this.populates = undefined;
        this.asc = undefined;
        this.desc = undefined;
    }

    public getCriteriaOrders(asc : Array<any>, desc : Array<any>, nomeCampoDefault: string) {
        let orderParams = [];
        if(asc && asc.length > 0) {
            asc = [].concat( asc);
            asc.forEach(element => {
                orderParams.push([element, 'ASC']);    
            });
        }
        if(desc && desc.length > 0) {
            desc = [].concat( desc);
            desc.forEach(element => {
                orderParams.push([element, 'DESC']);    
            });
        }
        if(orderParams.length == 0) {
            orderParams.push([nomeCampoDefault, 'ASC']);
        }
        return orderParams;
    }

    public calcRanges(){
        if(this.page > 0 && this.totalRows > 0){
            this.totalPages = Math.ceil(this.totalRows / this.limit);
        }
        if(this.page < this.totalPages){
            this.rangeEnd = this.page * this.limit;
        } else {
            this.rangeEnd = this.totalRows;
            this.page = this.totalPages;
        }
        if(this.page != this.totalPages){
            this.rangeInit = (this.rangeEnd + 1) - this.limit 
        } else{
            this.rangeInit =  ((this.totalPages - 1) * this.limit) + 1;

        }
    }

    public createFilters(object: Object, notWhere: Array<string>): any{
        console.log("Create");
        let obj = {};
        console.log(object);
        for (var key in object) {
            let value = object[key];
            let valueValid = value.replace(/\*/g, "");
            if(valueValid && notWhere.indexOf(key) < 0){
                console.log(value);
                this.setLike(value, obj, key);
            } 
        }
        return obj;
    }

    public setLike(value: string, obj: any, key: string){
        let likeInit: string = "", likeEnd: string = "";
        if(value.indexOf("*") == 0){
            value = value.substring(1);
            likeInit = ".*";
        }
        if(value.indexOf("*") == (value.length -1)){
            value = value.substring(0, value.length -1);
            likeEnd = ".*";
        }
        if(key.indexOf("_id") < 0 && (likeInit || likeEnd)){
            obj[key]  = { $regex: likeInit + value + likeEnd };
        }else{
            obj[key] = value;
        }
    }

    public getUrlPattern(versao: string, urlOriginal: string, isNoSql: boolean): string{
        let urlSemVersao = (urlOriginal.indexOf("?") > -1)
              ? urlOriginal.substring(versao.length + 1, urlOriginal.indexOf("?"))
              : urlOriginal.substring(versao.length + 1);
        let dominio = (urlSemVersao.indexOf("/") > -1) 
              ? urlSemVersao.substring(0, urlSemVersao.indexOf("/"))
              : urlSemVersao;
        let parametros = urlSemVersao.substring(dominio.length+1);
        let partes = parametros.split("/");
        parametros = "";
        if(partes && partes.length > 0){
            let valor = partes[0];
            if(valor && partes.length == 1){
                if(Validation.isOnlyNumbers(valor) || (isNoSql && Validation.containsNumbers(valor))){
                    parametros += "/:id"; 
                }else{
                    parametros += "/" + valor; 
                }
            }else{
                partes.forEach((p, index) =>{
                if(index > 0 && (index +1) % 2 == 0){
                    let campo = partes[index-1];
                    parametros +=  "/" + campo + "/:" + campo; 
                }
                });
          }
        }
        return versao + "/" + dominio + parametros;
    }

    public getActionsUser(usuario: any): Array<string>{
        let acoesUsuario: Array<string> = [];
        let perfis = usuario["perfis"];
        if(perfis){
            perfis.forEach(perfil => {
                let acoes = perfil["acoes"];
                if(acoes){
                    acoes.forEach(acao => {
                        let nomeTipoAcao = acao.url + "-" + acao.tipo;
                        if(acoes.indexOf(nomeTipoAcao) < 0){
                            acoesUsuario.push(nomeTipoAcao);
                        }
                    });
                }
            });
        }
        return acoesUsuario;
    }
}