import { tMsg } from './../../util/Values';
import { msgUsuario } from './../../util/MessageTranslate';
import { Data } from './../../util/Data';
import * as express from "express";
import UsuarioModel  from "./UsuarioModel";

class UsuarioService{
    public async getAll(req, res){
        let data = new Data();
        try {
            let filter = data.createFilter(req);
            console.log("Filter");
            console.log(filter);
            let query = UsuarioModel.find(filter);
            query.populate({
                path: "organizacoes", 
                populate: {
                    path: "filiais"
                }
            
            });
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoListarUsuarios);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            data.obj = await UsuarioModel.create(req.body);
            data.addMsg(tMsg.SUCCESS, msgUsuario.usuarioInseridoComSucesso);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoInserirUsuario);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await UsuarioModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgUsuario.usuarioAlteradoComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoAlterarUsuario);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await UsuarioModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgUsuario.usuarioRemovidoComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoRemoverUsuario);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        console.log(id);
        try {
            data.obj = await UsuarioModel.findById(id).populate("acoes", "nome url").populate({
                path: "organizacoes", 
                populate: {
                    path: "filiais"
                }
            }).populate({
                path: "perfis",
                populate:{
                    path: "acoes"
                }
            });
            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
       
}

export default new UsuarioService();