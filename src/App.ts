import { URLs } from './util/Values';
import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as cors from "cors";
import OrganizacaoService from './app/organizacao/OrganizacaoService';
import OrganizacaoModel from './app/organizacao/OrganizacaoModel';
import OrganizacaoRest from './app/organizacao/OrganizacaoRest';
import FilialRest from './app/filial/FilialRest';
import AcaoRest from './app/acao/AcaoRest';
import PerfilRest from './app/perfil/PerfilRest';
import EnderecoRest from './app/endereco/EnderecoRest';
import UsuarioRest from './app/usuario/UsuarioRest';
import AuthTokenRest from './app/authToken/AuthTokenRest';

// Criando as configurações para o ExpressJS
class App {
// Instancia dele
  public exp: express.Application;
  constructor() {
    this.exp = express();
    this.middleware();
    this.routes();
  }
// Configuração para o nosso middler
  private middleware(): void {
    this.exp.use(logger('dev'));
    this.exp.use(bodyParser.json());
    this.exp.use(bodyParser.urlencoded({ extended: true }));
    this.exp.use(express.static(path.join(__dirname, "public")));
    this.exp.use(cors());
  }
//Configuração da nossa API e nossos EndPoint e o famoso Hello 
  private routes(): void {   
    let router = express.Router();
  
    router.get('/', (req, res, next) => {
        console.log("TESTe");
      res.json({
        message: 'Hello World!'
      });
    });
    this.exp.use('/', router);
    AuthTokenRest.setRoutes(this.exp);
    OrganizacaoRest.setRoutes(this.exp);
    FilialRest.setRoutes(this.exp);
    AcaoRest.setRoutes(this.exp);
    PerfilRest.setRoutes(this.exp);
    EnderecoRest.setRoutes(this.exp);
    UsuarioRest.setRoutes(this.exp);

  
  }
}

export default new App();