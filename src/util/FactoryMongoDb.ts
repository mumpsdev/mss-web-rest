import * as mongoose from "mongoose";
class FactoryMongoDb{
    constructor(){
        // mongoogse.Promise(global.Promise);
    }
    //mongodb://<dbuser>:<dbpassword>@ds125479.mlab.com:25479/mss_db
    private url: string = "mongodb://";
    private host: string = "localhost";
    private user: string = "";
    private password = "";
    private port = "";
    private database = "mss_db";
    private options: mongoose.ConnectionOptions;
    private mongoo
    private db: any;
    private conn: any;

    public async createConnection(onConnected: any, onDisnnected: any, onError: any){
        let nodeEnv:string = process.env.NODE_ENV || "No environment";
        console.log("Node_ENV: " + nodeEnv);
        if(nodeEnv.indexOf("test") > -1){

        }else if(nodeEnv.indexOf("development") > -1){
            this.host = "@ds125479.mlab.com";
            this.port = "25479";
            this.user = "mumps";
            this.password = "mumps123456";
            this.options = {
                reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
                reconnectInterval: 500, // Reconnect every 500ms
                poolSize: 10, // Maintain up to 10 socket connections
                // If not connected, return errors immediately rather than waiting for reconnect
                bufferMaxEntries: 0
            }
        }else if(nodeEnv.indexOf("production") > -1){

        }
        console.log("Running in environment: " + nodeEnv);
        let urlConnection = this.getUrlConnection();
        mongoose.set('debug', true);
        //Conectando ao banco
        this.db = mongoose.connect(urlConnection, this.options);
        //Conexao
        this.conn = mongoose.connection;

        this.conn.on("connected", onConnected);
        
        this.conn.on("disconnected", onDisnnected);
        
        this.conn.on("error", onError);
    }

    public getUrlConnection(): string{
        let userpassword = (this.user && this.password) ? this.user + ":" + this.password : "";
        let port = (this.port) ? ":" + this.port : "";
        let urlConnection = this.url + userpassword + this.host + port + "/" + this.database;
        return urlConnection;
    }

    public getConnection(): mongoose.Connection{
        return this.conn;
    }

    public closeConnection(onClose: any){
        if(this.conn){
            this.conn.close(onClose);
        }
    }
}
export default new FactoryMongoDb();