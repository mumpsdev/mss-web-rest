import { URLs } from './../../util/Values';
import UsuarioService  from './UsuarioService';
import * as express from "express";

class UsuarioRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.USUARIO)
        .get(UsuarioService.getAll)
        .post(UsuarioService.create); 
        
        express.route(URLs.USUARIO_ID)
        .get(UsuarioService.getById)
        .delete(UsuarioService.remove)
        .put(UsuarioService.update);
    }
}
export default new UsuarioRest();