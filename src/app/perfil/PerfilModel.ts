import * as mongoose from "mongoose";

export let PerfilSchema = new mongoose.Schema({
    nome:{
        type: String,
        required: true,
        unique: true
    },
    filial: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Filial"
    },

    organizacao: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "organizacao"
    },

    acoes:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Acao"
    }]
});

export default mongoose.model("Perfil", PerfilSchema);