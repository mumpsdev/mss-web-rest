import { tMsg } from './../../util/Values';
import { msgOrganizacao } from './../../util/MessageTranslate';
import { Data } from './../../util/Data';
import * as express from "express";
import OrganizacaoModel  from "./OrganizacaoModel";

class OrganizacaoService{
    public async getAll(req, res){
        let data = new Data();
        try {
            let filter = data.createFilter(req);
            let query = OrganizacaoModel.find(filter);
            query.populate("filiais");
            await data.executeQuery(query);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoListarOrganizacoes);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            data.obj = await OrganizacaoModel.create(req.body)
            data.addMsg(tMsg.DANGER, msgOrganizacao.organizacaoInseridaComSucesso);
            res.status(200).json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoInserirOrganizacao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await OrganizacaoModel.findByIdAndUpdate(id, req.body, {new: true});
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgOrganizacao.organizacaoAlteradaComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoLocalizarOrganizacao);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoAlterarOrganizacao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await OrganizacaoModel.findByIdAndRemove(id);
            if(data.obj){
                data.addMsg(tMsg.DANGER, msgOrganizacao.organizacaoRemovidaComSucesso);
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoLocalizarOrganizacao);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoRemoverOrganizacao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }

    public async getById(req: express.Request, res: express.Response){
        let data = new Data();
        let id = req.params.id;
        try {
            let query = OrganizacaoModel.findById(id);
            query.populate("filiais", "fantasia");
            data.obj = await query.exec("find");

            if(data.obj){
                res.status(200).json(data);
            }else{
                data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoLocalizarOrganizacao);
                res.status(404).json(data);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoLocalizarOrganizacao);
            console.log("Erro: " + error);
            res.status(500).json(data);
        }
    }
}

export default new OrganizacaoService();