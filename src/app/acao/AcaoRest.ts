import { URLs } from './../../util/Values';
import AcaoService  from './AcaoService';
import * as express from "express";

class AcaoRest{

    public setRoutes(express: express.Application){
        //Definindo as rotas.
        express.route(URLs.ACAO)
        .get(AcaoService.getAll)
        .post(AcaoService.create); 
        
        express.route(URLs.ACAO_ID)
        .get(AcaoService.getById)
        .delete(AcaoService.remove)
        .put(AcaoService.update);
    }
}
export default new AcaoRest();